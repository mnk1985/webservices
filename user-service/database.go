package main

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func CreateConnection() (*gorm.DB, error) {

	// Get database details from environment variables

	host := os.Getenv("MYSQL_HOST")
	port := os.Getenv("MYSQL_PORT")
	user := os.Getenv("MYSQL_USER")
	DBName := os.Getenv("MYSQL_DATABASE")
	password := os.Getenv("MYSQL_PASSWORD")
	dsn := user + ":" + password + "@tcp(" + host + ":" + port + ")/" + DBName + "?charset=utf8&parseTime=True&loc=Local"
	fmt.Println("dsn = " + dsn)
	return gorm.Open(
		"mysql",
		dsn,
	)
}
